#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p initramfs_files/{dev,mnt,proc,sys,tmp,var}
sh -c 'cd initramfs_files/ && find . | cpio -H newc -o' | gzip -9 > new_initramfs.cpio.gz
mkimage -A arm64 -O linux -T ramdisk -C gzip -d new_initramfs.cpio.gz initramfs
rm -rf new_initramfs.cpio.gz
