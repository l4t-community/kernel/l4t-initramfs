#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p initramfs_files
cd initramfs_files
tail -c+65 < ../initramfs | gunzip | cpio -idmv
